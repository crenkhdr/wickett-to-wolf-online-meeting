var menuIcon = "fa-angle-double-right";

var currSlide = 0;
var maxSlides;
var $screen = $("div#screen");
var $screenCover = $("a#menuOffScreen");
var $slideBox = $("div#slides > div");
var $slideItem = $("div#slides > div > div");

$(function() {
     maxSlides = $slideItem.length - 1;

     /* Initialize sizes and populate lists based on amount of slides present */
     $slideBox.css({width: (100 * (maxSlides + 1)) + "%"});
     $slideItem.css({width: (100 / (maxSlides + 1)) + "%"});
     for( var i = 0; i <= maxSlides; i++ ) {
          $("div#topArea ul").append("<li></li>");
          $("div#menu ul").append( '<li><a href="javascript:moveSlide(' + i + ');"><i class="fa ' + 
                    menuIcon +
               '"></i><span>' +
                    $slideItem.eq(i).find("h2").html() +
               '</span></a></li>');
     }
     $("div#topArea li").each(function(i) {
          $(this).on("click", function() { 
               moveSlide(i); 
          });
     });
     $("div#topArea li").eq(0).addClass("showing"); 
     $("div#menu ul li a i").eq(0).addClass("showing");  
     $("a.left-arrow").hide();

     /* Initialize Lightbox for Images */
     $(".lightbox").on("click", launchLightBox);
     $("#lightbox span").on("click", function() {
          $("div#lightbox").removeClass("on");
     });

     /** Set up menu arrows **/
     $("div#menu div").on("scroll", checkScroll);
     if( $("div#menu ul").height() > $("div#menu div").height() ) {
          $(".menuArrow").show();
          $(".top-arrow").on("mousedown", function() { setMenu(-1); });
          $(".bottom-arrow").on("mousedown", function() { setMenu(1); });
     } else { 
          $(".menuArrow").hide(); 
     }

     /** Screen reset */
     $("#menuOffScreen, .closebtn").on("click", function() { 
          $screen.stop().animate({left: "0"}, 400); 
          $("#contact").stop().animate({ marginLeft: "0"}, 400);
          $("#signIn").stop().animate({ marginLeft: "0"}, 400);
          $screenCover.fadeOut(200);  
          $("a.menu-toggle").removeClass("opened"); 
     });

     // MOVE SLIDE WITH KEYBOARD
     document.onkeydown = function(evt) {
          evt = evt || window.event;
          switch (evt.keyCode) {
          case 37:
               setArrow(-1);
               break;
          case 39:
               setArrow(1);
               break;
          }
     };

     // Accordion Scripts
     var acc = document.getElementsByClassName("accordion");
     var i;
     
     for (i = 0; i < acc.length; i++) {
       acc[i].addEventListener("click", function() {
         this.classList.toggle("active");
         var panel = this.nextElementSibling;
         if (panel.style.maxHeight) {
           panel.style.maxHeight = null;
         } else {
           panel.style.maxHeight = panel.scrollHeight + "px";
         }
       });
     }
});

function stopAudio() {
     var sounds = document.getElementsByTagName("audio");
     for( i = 0; i < sounds.length; i++ ) {
          sounds[ i ].pause();
     }
     sounds = document.getElementsByTagName("video");
     for( i = 0; i < sounds.length; i++ ) {
          sounds[ i ].pause();
     }
}

function setMenu(dir) {
    if( ( dir == 1 && !($(".bottom-arrow").hasClass("disabled")) ) ||
          ( dir == -1 && !($(".top-arrow").hasClass("disabled")) ) ) {
               checkScroll();
               $("div#menu div").animate({ scrollTop: $("div#menu div").scrollTop() + (100 * dir) }, 420);
          }
}

function checkScroll() {
    $(".menuArrow").removeClass("disabled");
    if( $("div#menu div").scrollTop() == 0 ) { 
         $(".top-arrow").addClass("disabled"); 
    }
    if( $("div#menu div").scrollTop() > ( $("div#menu ul").height() - $("div#menu div").height() - 1 ) ) { 
         $(".bottom-arrow").addClass("disabled"); 
    }
}

function moveSlide(no) {
     stopAudio();
    /* Move slide */
    var className = ( no > currSlide ) ? "slide-right" : "slide-left";
    $slideBox.css({ left: -(no * 100) + "%" }).parent().addClass(className);
    setTimeout(function() { $slideBox.parent().removeClass(className); }, 950);
    currSlide = no;  
    $("div#topArea li").removeClass("showing").eq(currSlide).addClass("showing");
    $("div#menu ul li a i").removeClass("showing").eq(currSlide).addClass("showing");

    /* Set Arrow visibility */
    if(currSlide == 0) { $("a.left-arrow").hide(); } else { $("a.left-arrow").show(); }
    if(currSlide == maxSlides) { $("a.right-arrow").hide(); } else { $("a.right-arrow").show(); }

    /* Move image behind slides */
    $("div#mainArea > img").css({ marginLeft: (12 * ((no/maxSlides) - 1)) + "%" });

    /* Move menu on left to show current slide */
    $("#menu div").animate({ scrollTop: ( (42 * no) - (.5 * $("#menu div").height()) + 60 )}, 750);
}

function setArrow(dir) {
    var newIdx = currSlide + dir;
    if(newIdx == -1) { newIdx = maxSlides; }
    if(newIdx == maxSlides + 1) { newIdx = 0; }
    moveSlide(newIdx);
}

/* Manipulate side menus */
function openMenu() {
    $("a.menu-toggle").addClass("opened"); 
    $screenCover.fadeIn(450); 
    $screen.stop().animate({left: "240px"}, 550);
}
function openContact() {
    var wd = ( $(window).width() < 640 ) ? "-280px" : "-400px";
    $screenCover.fadeIn(450); 
    $screen.stop().animate({left: wd}, 550);
    $("#contact").stop().animate({ marginLeft: wd}, 550);
}

function opensignIn() {
     var wd = ( $(window).width() < 640 ) ? "-280px" : "-400px";
     $screenCover.fadeIn(450); 
     $screen.stop().animate({left: wd}, 550);
     $("#signIn").stop().animate({ marginLeft: wd}, 550);
 }

/* Manipulate light box */
function launchLightBox(e) {
    $("div#lightbox").addClass("on").find("img").attr("src", $(e.target).attr("src") );
}